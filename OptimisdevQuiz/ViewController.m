//
//  ViewController.m
//  OptimisdevQuiz
//
//  Created by NewWay on 10/8/15.
//  Copyright (c) 2015 NewWay. All rights reserved.
//

#import "ViewController.h"
#import "GitRepoTableViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize organizationTextField;
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
//    NSLog( @"%s", __PRETTY_FUNCTION__ );
    
    if( [organizationTextField.text isEqualToString:@""] )
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"ERROR"
                                                            message:@"Empty Organization"
                                                           delegate:nil
                                                  cancelButtonTitle:@"Try Again"
                                                  otherButtonTitles:nil];
        [alertView show];
        return NO;
    }
    return YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
//    NSLog( @"%s", __PRETTY_FUNCTION__ );
    
    GitRepoTableViewController *repoTableViewController = (GitRepoTableViewController*)segue.destinationViewController;
    repoTableViewController.organizationName = organizationTextField.text;
    return;
}
@end
