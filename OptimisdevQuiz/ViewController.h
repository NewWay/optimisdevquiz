//
//  ViewController.h
//  OptimisdevQuiz
//
//  Created by NewWay on 10/8/15.
//  Copyright (c) 2015 NewWay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic, weak) IBOutlet UITextField *organizationTextField;

@end

