//
//  GitRepoTableViewController.h
//  OptimisdevQuiz
//
//  Created by NewWay on 10/8/15.
//  Copyright (c) 2015 NewWay. All rights reserved.
//

#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT,0)
#define kGitapiPreURL [NSURL URLWithString:@"https://api.github.com/"]

#import <UIKit/UIKit.h>

@interface GitRepoTableViewController : UITableViewController
{
    
}
@property (strong, nonatomic) NSString *organizationName;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicatorView;
@end
