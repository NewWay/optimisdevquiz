//
//  CalendarViewController.m
//  OptimisdevQuiz
//
//  Created by NewWay on 10/8/15.
//  Copyright (c) 2015 NewWay. All rights reserved.
//

#import "GitRepoTableViewController.h"
#import "CalendarViewController.h"
#import "DSLCalendarView.h"
#import "DSLCalendarDayView.h"
#import "DSLCalendarMonthView.h"

@interface CalendarViewController () <DSLCalendarViewDelegate>
@property (nonatomic, weak) IBOutlet DSLCalendarView *calendarView;
- (void) fetchedData:(NSData*)responseData;
@end

@implementation CalendarViewController
{
    NSDictionary *commitsDictionary;
    NSArray *createTimeArray;
    NSArray *typeArray;
    
    NSArray *commitDateArray;
}

@synthesize repoName;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"Activities";

//    https://api.github.com/repos/facebook/android-jsc/events
    NSString *commitString = [NSString stringWithFormat:@"%@repos/%@/commits", kGitapiPreURL, repoName];
    NSLog( @"activitiesString = %@", commitString );
    NSURL *url = [NSURL URLWithString:commitString];
    
    dispatch_async( kBgQueue, ^(void){
        NSData *data = [NSData dataWithContentsOfURL:url];
        [self performSelectorOnMainThread:@selector(fetchedData:)
                               withObject:data
                            waitUntilDone:YES];
    });
    
    CGRect frame = CGRectMake(self.view.bounds.origin.x,
                              self.view.bounds.origin.y + 60,
                              self.view.bounds.size.width,
                              self.view.bounds.size.height - 60);
    self.calendarView = [[DSLCalendarView alloc] initWithFrame:frame];
    [self.view addSubview:self.calendarView];
    self.calendarView.delegate = self;
    
//    self.textLabel.text = [NSString stringWithFormat:@"%@ Commit Date", repoName];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) fetchedData:(NSData*)responseData
{
    if( responseData == nil )
    {
        NSLog( @"responseData == nil" );
        NSString *errorMsg = [NSString stringWithFormat:@"No commits for %@", repoName];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"ERROR"
                                                            message:errorMsg
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
        
        return;
    }
    
    
    NSError *error;
    commitsDictionary = [NSJSONSerialization JSONObjectWithData:responseData
                                                        options:kNilOptions
                                                          error:&error];
    
    // 有時候 responseData 不會是nil，會是 '[]' 值, 所以再用 NSDictionary.count 值來判斷是不是沒有repo
    if( commitsDictionary.count == 0 )
    {
//        NSLog( @"repoDictionary.count == 0" );
        NSString *errorMsg = [NSString stringWithFormat:@"No commits for %@", repoName];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"ERROR"
                                                            message:errorMsg
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
        
        return;
    }

//    createTimeArray = [activitiesDictionary valueForKey:@"created_at"];
//    typeArray = [activitiesDictionary valueForKey:@"type"];
//    
//    NSLog( @"%@", createTimeArray );
//    NSLog( @"%@", typeArray );
    
    NSArray *array = [commitsDictionary valueForKey:@"commit"];
    NSArray *authorArray = [array valueForKey:@"author"];
    
    commitDateArray = [authorArray valueForKey:@"date"];
    
    NSLog( @"%@", commitDateArray );
    NSLog( @"commitDateArray count = %d", [commitDateArray count] );

    NSDateComponents *dateComponentsForDate = [[NSCalendar currentCalendar] components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSWeekdayCalendarUnit | NSCalendarCalendarUnit fromDate:[NSDate date]];
    
    for( NSString *string in commitDateArray )
    {
        [dateComponentsForDate setYear:[[string substringWithRange:NSMakeRange (0, 4)] intValue]];
        [dateComponentsForDate setMonth:[[string substringWithRange:NSMakeRange (5, 2)] intValue]];
        [dateComponentsForDate setDay:[[string substringWithRange:NSMakeRange (8, 2)] intValue]];
        
//        NSLog( @"dateComponent = %@", dateComponentsForDate );
//        NSLog( @"visibleMonth = %@", self.calendarView.visibleMonth );
        
        
        DSLCalendarRange *range = [[DSLCalendarRange alloc] initWithStartDay:dateComponentsForDate
                                                                      endDay:dateComponentsForDate];
        
        
        
//        NSString *monthViewKey = [self.calendarView monthViewKeyForMonth:dateComponentsForDate];
////        NSLog( @"monthViewKey = %@", monthViewKey );
//        
//        DSLCalendarMonthView *monthView = [self.calendarView.monthViews objectForKey:monthViewKey];
//        NSLog( @"monthView = %@", monthView );
//        
//        
//        DSLCalendarDayView *dayView = [monthView dayViewForDay:dateComponentsForDate];
//        
//        [dayView setBackgroundColor:[UIColor redColor]];
//        [monthView setNeedsDisplay];
//        [dayView setNeedsDisplay];
//        
//        NSLog( @"dateView = %@", [dayView dayAsDate] );
        
        [self.calendarView setSelectedRange:range];
    }
    
//    NSDateComponents *dateComponentsForDate = [[NSDateComponents alloc] init];
//    [dateComponentsForDate setYear:2015];
//    [dateComponentsForDate setMonth:10];
//    [dateComponentsForDate setDay:1];
//     
//    DSLCalendarRange *range = [[DSLCalendarRange alloc] initWithStartDay:dateComponentsForDate
//                                                                  endDay:dateComponentsForDate];
    
//    [self.calendarView setVisibleMonth:self.calendarView.visibleMonth animated:YES];
    
//    [self.calendarView setDraggingStartDay:dateComponentsForDate];
    
//    [self.calendarView setSelectedRange:range];
//    
//    [self calendarView:self.calendarView didSelectRange:range];

    
//    DSLCalendarMonthView *monthView = (DSLCalendarMonthView*)[[self.calendarView class] monthViewClass];
//    [monthView updateDaySelectionStatesForRange:range];
    
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

//#pragma mark - DSLCalendarViewDelegate methods
//
//- (void)calendarView:(DSLCalendarView *)calendarView didSelectRange:(DSLCalendarRange *)range {
//    if (range != nil) {
//        NSLog( @"Selected %ld/%ld/%ld - %ld/%ld/%ld", (long)range.startDay.day, (long)range.startDay.month, (long)range.startDay.year, (long)range.endDay.day, (long)range.endDay.month, (long)range.endDay.year );
//    }
//    else {
//        NSLog( @"No selection" );
//    }
//}
//
//- (DSLCalendarRange*)calendarView:(DSLCalendarView *)calendarView didDragToDay:(NSDateComponents *)day selectingRange:(DSLCalendarRange *)range {
//    if (NO) { // Only select a single day
//        return [[DSLCalendarRange alloc] initWithStartDay:day endDay:day];
//    }
//    else if (NO) { // Don't allow selections before today
//        NSDateComponents *today = [[NSDate date] dslCalendarView_dayWithCalendar:calendarView.visibleMonth.calendar];
//        
//        NSDateComponents *startDate = range.startDay;
//        NSDateComponents *endDate = range.endDay;
//        
//        if ([self day:startDate isBeforeDay:today] && [self day:endDate isBeforeDay:today]) {
//            return nil;
//        }
//        else {
//            if ([self day:startDate isBeforeDay:today]) {
//                startDate = [today copy];
//            }
//            if ([self day:endDate isBeforeDay:today]) {
//                endDate = [today copy];
//            }
//            
//            return [[DSLCalendarRange alloc] initWithStartDay:startDate endDay:endDate];
//        }
//    }
//    
//    return range;
//}
//
- (void)calendarView:(DSLCalendarView *)calendarView willChangeToVisibleMonth:(NSDateComponents *)month duration:(NSTimeInterval)duration {
//    NSLog(@"Will show %@ in %.3f seconds", month, duration);
}

- (void)calendarView:(DSLCalendarView *)calendarView didChangeToVisibleMonth:(NSDateComponents *)month {
//    NSLog(@"Now showing %@", month);
    NSDateComponents *dateComponentsForDate = [[NSCalendar currentCalendar] components:NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSWeekdayCalendarUnit | NSCalendarCalendarUnit fromDate:[NSDate date]];
    
    for( NSString *string in commitDateArray )
    {
        [dateComponentsForDate setYear:[[string substringWithRange:NSMakeRange (0, 4)] intValue]];
        [dateComponentsForDate setMonth:[[string substringWithRange:NSMakeRange (5, 2)] intValue]];
        [dateComponentsForDate setDay:[[string substringWithRange:NSMakeRange (8, 2)] intValue]];
        
        DSLCalendarRange *range = [[DSLCalendarRange alloc] initWithStartDay:dateComponentsForDate
                                                                      endDay:dateComponentsForDate];
        
        [self.calendarView setSelectedRange:range];
    }
}

- (BOOL)day:(NSDateComponents*)day1 isBeforeDay:(NSDateComponents*)day2 {
    return ([day1.date compare:day2.date] == NSOrderedAscending);
}


@end
