//
//  CalendarViewController.h
//  OptimisdevQuiz
//
//  Created by NewWay on 10/8/15.
//  Copyright (c) 2015 NewWay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CalendarViewController : UIViewController
@property (nonatomic, strong) NSString *repoName;
@property (nonatomic, weak) IBOutlet UILabel *textLabel;
@end
