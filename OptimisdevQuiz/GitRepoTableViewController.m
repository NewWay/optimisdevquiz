//
//  GitRepoTableViewController.m
//  OptimisdevQuiz
//
//  Created by NewWay on 10/8/15.
//  Copyright (c) 2015 NewWay. All rights reserved.
//

#import "GitRepoTableViewController.h"
#import "CalendarViewController.h"

@interface GitRepoTableViewController ()
- (void) fetchedData:(NSData*)responseData;
@end

@implementation GitRepoTableViewController
{
    NSDictionary *repoDictionary;
    NSArray *repoNameArray;
    NSArray *repoDescriptionArray;
}
@synthesize organizationName;
@synthesize indicatorView;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
//    repoDictionary = [[NSDictionary alloc] init];
    NSString *urlString = [NSString stringWithFormat:@"%@users/%@/repos",kGitapiPreURL, organizationName];
    
    NSLog( @"urlString = %@", urlString );
    
    NSURL *repoUrl = [NSURL URLWithString:urlString];
    
    dispatch_async( kBgQueue, ^(void){
        
        NSData *data = [NSData dataWithContentsOfURL:repoUrl];
        [self performSelectorOnMainThread:@selector(fetchedData:) withObject:data waitUntilDone:YES];
    });
    
    self.navigationItem.title = @"Repositories";
    
    [indicatorView setHidden:NO];
    [indicatorView startAnimating];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) fetchedData:(NSData*)responseData
{
//    NSString *string = [[NSString alloc] initWithData:responseData encoding:NSUTF8StringEncoding];
//    NSLog( @"string = %@", string );
    
    if( responseData == nil )
    {
        NSLog( @"responseData == nil" );
        NSString *errorMsg = [NSString stringWithFormat:@"No repo for %@", organizationName];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"ERROR"
                                                            message:errorMsg
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
        [indicatorView setHidden:YES];
        
        return;
    }
    
//    UIActivityIndicatorView *indicatoeView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
//    [self.tableView addSubview:indicatoeView];
//    [indicatoeView startAnimating];
    
    
    NSError *error;
    repoDictionary = [NSJSONSerialization JSONObjectWithData:responseData
                                                     options:kNilOptions
                                                       error:&error];
    
    // 有時候 responseData 不會是nil，會是 '[]' 值, 所以再用 NSDictionary.count 值來判斷是不是沒有repo
    if( repoDictionary.count == 0 )
    {
        NSLog( @"repoDictionary.count == 0" );
        NSString *errorMsg = [NSString stringWithFormat:@"No repo for %@", organizationName];
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"ERROR"
                                                            message:errorMsg
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
        [indicatorView setHidden:YES];
        
        return;
    }
//    NSArray *array = [repoDictionary valueForKey:@"full_name"];
//    NSLog( @"%@", array );
    repoNameArray = [repoDictionary valueForKey:@"full_name"];
    repoDescriptionArray = [repoDictionary valueForKey:@"description"];
    
    [self.tableView reloadData];
    [indicatorView stopAnimating];
    [indicatorView setHidden:YES];
//    [indicatoeView removeFromSuperview];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return repoDictionary.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"SimpleTableCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if( cell == nil ) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    
    // Configure the cell...
    cell.textLabel.text = (NSString*)[repoNameArray objectAtIndex:indexPath.row];
    cell.detailTextLabel.text = (NSString*)[repoDescriptionArray objectAtIndex:indexPath.row];
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here. Create and push another view controller.
    /*
     <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:@"<#Nib name#>" bundle:nil];
     // ...
     // Pass the selected object to the new view controller.
     [self.navigationController pushViewController:detailViewController animated:YES];.0
     
     */
    
    [self performSegueWithIdentifier:@"showCalendar" sender:nil];
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/


#pragma mark - Navigation

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    NSLog( @"%s", __PRETTY_FUNCTION__ );
    return YES;
}
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    NSLog( @"%s", __PRETTY_FUNCTION__ );
    
    if( [segue.identifier isEqualToString:@"showCalendar"] )
    {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        CalendarViewController *calendarViewController = (CalendarViewController*)segue.destinationViewController;
        calendarViewController.repoName = (NSString*)[repoNameArray objectAtIndex:indexPath.row];
        
        NSLog( @"calendarViewController.repoName = %@", calendarViewController.repoName );
    }
}


@end
